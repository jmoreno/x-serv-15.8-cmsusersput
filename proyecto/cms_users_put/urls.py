from django.urls import path
from . import views

urlpatterns = [
	path('',views.index),
	path('loggedIn', views.loggedIn),
    	path('loggedOut', views.loggedOut),
	path('<str:llave>',views.get_content),
]

