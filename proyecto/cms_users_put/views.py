from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect


from .models import Contenido

# Create your views here.

form = """
<form action="" method="POST">
	Valor: <input type="text" name="valor">
	</br><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):

	# -----> POST:
	if request.method == "POST":
		try:
			# Cogemos el valor de cuerpo y lo almacenamos en una variable
			value = request.body.decode('utf-8')
				
			# Tomamos el valor a actualizar
			content = Contenido.objects.get(clave=llave)
				
			# Eliminamos el valor antiguo de la base de datos
			content.delete()
				
			# Actualizamos el valor en la base de datos
			content= Contenido(clave=llave, valor=value)
				
			# Guardamos el valor obtenido en la base de datos
			content.save()
		
		# EXCEPCIÓN: se lanzará cuando la clave no exista, y hará que se guarde en la base de datos	
		except Contenido.DoesNotExist:
			content= Contenido(clave=llave, valor=value)
			
			# Guardamos el valor obtenido en la base de datos
			content.save()
	
	# -----> PUT:	
	if request.method == "PUT":
		try:
			# Cogemos el valor de cuerpo y lo almacenamos en una variable
			value = request.body.decode('utf-8')
			
			# Tomamos el valor a actualizar
			content = Contenido.objects.get(clave=llave)
			
			# Eliminamos el valor antiguo de la base de datos
			content.delete()
			
			# Actualizamos el valor en la base de datos
			content= Contenido(clave=llave, valor=value)
			
			# Guardamos el valor obtenido en la base de datos
			content.save()
		
		# EXCEPCIÓN: se lanzará cuando la clave no exista, y hará que se guarde en la base de datos	
		except Contenido.DoesNotExist:
			content= Contenido(clave=llave, valor=value)
			
			# Guardamos el valor obtenido en la base de datos
			content.save()
	
	try:
		# Mostramos el contenido de la llave en la página
		content = Contenido.objects.get(clave=llave)
		response = "El valor de la llave es : " + content.valor
	
	# EXCEPCIÓN: se lanzará cuando la clave no exista, y hará que se cargue el formulario en pantalla
	except Contenido.DoesNotExist:
		response=form
		
	return HttpResponse(response)
	
def index(request):
	content_list = Contenido.objects.all()
	template = loader.get_template('cms/index.html')
	context = {
		'content_list' : content_list
	}
	
	
	return HttpResponse(template.render(context,request))
	
@csrf_exempt
def loggedIn (request):
    if request.user.is_authenticated:
        response = "Bienvenido, " + request.user.username
    else:
        response = "Lo sentimos, el usuario introducido no se encuentra en la base de datos <a href='/login'>Haz click aquí para intentalo de nuevo</a>"
    return HttpResponse (response)

@csrf_exempt
def loggedOut (request):
    logout(request)
    return redirect('/cms/')

