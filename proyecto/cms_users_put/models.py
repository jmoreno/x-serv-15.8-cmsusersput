from django.db import models

# Create your models here.

class Contenido(models.Model):
	clave = models.CharField(max_length=64)
	valor = models.CharField(max_length=64)
	def __str__(self):
        	return str(self.id) + ": " + self.clave + " -- " + self.valor

