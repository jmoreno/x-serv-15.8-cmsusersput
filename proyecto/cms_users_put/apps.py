from django.apps import AppConfig


class CmsUsersPutConfig(AppConfig):
    name = 'cms_users_put'
